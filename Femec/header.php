<!--<header id="topHead">
          <!--<span class="quick-contact pull-left">
            <a class="hidden-xs" href="mailto:femeckoinonia@hotmail.com"><i class="fa fa-envelope"></i> femeckoinonia@hotmail.com</a>
        </span>

        <!-- <div class="pull-right nav signin-dd">
            <a id="quick_sign_in" href="page-signin.html" data-toggle="dropdown">
                <i class="fa fa-users"></i>
                <span class="hidden-xs"> Entrar</span>
            </a>
            <div class="dropdown-menu" role="menu" aria-labelledby="quick_sign_in">
                <h4>Área Administrativa</h4>
                <form action="#" method="post" role="form">
                    <div class="form-group">
                        <input required type="email" class="form-control" placeholder="Login">
                    </div>
                    <div class="input-group">
                        <input required type="password" class="form-control" placeholder="Senha">
                        <span class="input-group-btn">
                            <button class="btn btn-primary">Entrar</button>
                        </span>
                    </div>
                </form>
                <hr />
            </div>
        </div>
        <div class="pull-right nav hidden-xs">
            <a href="#footer-page"><i class="fa fa-angle-right"></i> Contatos</a>
        </div>
    </div>
</header>-->

<header id="topNav" >
    <div class="container">
        <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
            <i class="fa fa-bars"></i>
        </button>

        <a class="logo" href="/">
            <img src="assets/images/logo.png" alt="Femec Koinonia">
        </a>

        <div class="navbar-collapse nav-main-collapse collapse pull-right">
            <nav class="nav-main mega-menu">
                <ul class="nav nav-pills nav-main scroll-menu" id="topMain">
                    <li class="active">
                        <a href="/"><b>Início</b></a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#"><b>Quem Somos</b><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/quem-somos"><b>Princípios</b></a></li>
                            <li><a href="/diretoria"><b>Diretoria</b></a></li>
                            <li><a href="/departamentos"><b>Departamentos</b></a></li>
                            
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#"><b>UIECB</b><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/uiecb-historia"><b>Sua história</b></a></li>
                            <li><a href="/comec"><b>COMEC</b></a></li>
                            <li><a href="/dem"><b>DEM</b></a></li>
                            <li><a href="/nilson-braga"><b>Projeto Nilson Braga</b></a></li>
                            <li><a href="/colheita"><b>Projeto Colheita</b></a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#"><b>Conteúdos</b><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/noticias"><b>Notícias</b></a></li>
                            <li><a href="/reflexoes"><b>Reflexões</b></a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#"><b>Eventos</b><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/congresso-movase"><b>Congresso MOVA-SE</b></a></li>
                            <li><a href="/culto-femec-lute"><b>Culto FEMEC: LUTE</b></a></li>
                        </ul>

                    </li>
                    <li>
                        <a href="/fale-conosco"><b>Fale Conosco</b></a>
                    </li>
                    <!--<li class="search">
                        <form method="get" action="#" class="input-group pull-right">
                            <input type="text" class="form-control" name="k" id="k" value="" placeholder="Pesquisar">
                            <span class="input-group-btn">
                                <button class="btn btn-primary notransition"><i class="fa fa-search"></i></button>
                            </span>
                        </form>
                    </li>-->
                </ul>
            </nav>
        </div>
    </div>
</header>

<span id="header_shadow"></span>
