<div id="wrapper" class="bg-white">
    <section class="container">
        <h1>Conheça um pouco sobre a <b>COMEC</b></h1><br></BR>

        <div class="container">
            <section>
                <blockquote>COMEC é a <b>Confederação da Mocidade Evangélica Congregacional do Brasil (COMEC)</b>, com ilimitado tempo de duração, tem sede e foro que coincida com o da Junta Geral (UIECB. A Confederação tem pôr fim estimular, através das Federações, as Uniões locais das Igrejas filiadas à UIECB ao desempenho de tarefas comuns, nas suas atividades espirituais e temporais, promovendo o espírito de fraternidade e companheirismo entre elas, dentro dos princípios Congregacionais, visando, em tudo, o progresso do Reino de Deus.</blockquote>
            </section>
            
            <div class="divider"></div>

            <section>
                <h1>DIRETORIA COMEC</h1>
                <blockquote>A COMEC é dirigida por uma diretoria composta de: Presidente, 1º e 2º Vice Presidentes; 1º 2º e 3º Secretários; 1º e 2º Tesoureiros. Os membros da diretoria da COMEC serão eleitos em Congresso Nacional e terão mandato de 3 (três) anos. A diretoria atual foi eleita no último congresso nacional realizado no Rio de Janeiro-RJ em abril de 2015.</blockquote>
            </section>

            <!--PRESIDENTE-->
            <div class="row"><br>
                <div class="col-md-6">
                    <div class="department">
                        <div class="row"><br>

                            <figure class="col-md-4 col-md-offset-2 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/matheus.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Matheus Chimon</strong><br>
                                        <strong>Presidente</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/leonardo.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Leonardo Bezerra</strong><br>
                                        <strong>Vice-Presidente</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="department">
                        <div class="row"><br> 
                            <!--SECRETARIAS-->
                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/cintia.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Cintia Ferreira</strong><br>
                                        <strong>1ª Secretária</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/raquel.jpg"  class="img-rounded img-responsive col-md-2">
                                    <div class="">
                                        <strong>Raquel Lima</strong><br>
                                        <strong>2ª Secretária</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/michelle.jpg"  class="img-rounded img-responsive col-md-2">
                                    <div class="">
                                        <strong>Michelle Sant</strong><br>
                                        <strong>2ª Tesoureira</strong>
                                    </div>
                                </figcaption>
                            </figure>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row"><br>
                <div class="col-md-6">
                    <div class="department">
                        <div class="row"><br>

                            <!--SECRETÁRIOS REGIÕES-->

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/mariana.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Mariana Pereira</strong><br>
                                        <strong>Secretária Sudeste</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/marcos.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Marcos Paulo</strong><br>
                                        <strong>Secretário Nordeste</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/comec/arthur.jpg"  class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Arthur Farias</strong><br>
                                        <strong>Secretário Centro-Oeste</strong>
                                    </div>
                                </figcaption>
                            </figure>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
