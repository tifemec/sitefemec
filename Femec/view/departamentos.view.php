<div id="wrapper" class="bg-white">
<section class="container">
    <h1>Conheça nossos <b>Departamentos</b></h1></br>

    <div class="row">
        <div class="col-md-6">
            <!-- AJUNTOS  -->
            <div class="department">
                <h3>Adjuntos</h3>
                <blockquote>Representam a presidência da FEMEC na sua região de atuação, reportando-se previamente por relatórios e reuniões. Além disso, acompanham e auxiliam na realização das atividades agendadas no Plano Diretor.</blockquote>
                <!-- AJUNTOS RECIFE -->
                <div class="row">
                    <figure class="col-md-4 col-md-offset-2 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/adriana.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Adriana Paula</strong><br>
                                <strong>Recife e Zona da Mata</strong>
                            </div>
                        </figcaption>
                    </figure>

                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/karla.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Karla Ribeiro</strong><br>
                                <strong>Recife e Zona da Mata</strong>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <!-- AJUNTOS JABOATAO -->
                <div class="row">
                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/joyce.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Joyce Souza</strong><br>
                                <strong>Jaboatão</strong>
                            </div>
                        </figcaption>
                    </figure>

                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/mylena.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Mylena Souza</strong><br>
                                <strong>Jaboatão</strong>
                            </div>
                        </figcaption>
                    </figure>

                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/ricart2.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Ricart</strong><br>
                                <strong>Jaboatão</strong>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <!-- EVENTOS -->
        <div class="col-md-6">
            <div class="department">
                <h3 class="text-center">Eventos</h3>
                <blockquote>Responsáveis por atividades relacionadas a logística, ornamentação, alimentação, limpeza, publicação e convites.</blockquote>
                
                <div class="row">
                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/beatriz.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Beatriz Almeida</strong><br>
                            </div>
                        </figcaption>
                    </figure>
                    
                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/ane.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Oziane Bispo</strong>
                            </div>
                        </figcaption>
                    </figure>

                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/veronica.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Verônica Cabral</strong><br>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="row">
                    <figure class="col-md-4 col-md-offset-2 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/betinho.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Bettinho Silva</strong><br>
                            </div>
                        </figcaption>
                    </figure>
                    
                    <figure class="col-md-4 text-center">
                        <figcaption>
                            <img src="assets/images/diretoria/filipe.jpg" class="img-rounded img-responsive">
                            <div class="">
                                <strong>Filipe Luiz</strong>
                            </div>
                        </figcaption>
                    </figure>
                </div>

            </div>
        </div>
    </div> <br>

    <div class="row">
        <!-- MARKETING  -->
        <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Marketing</h3>
                    <blockquote>Atua na confecção de camisas, artes, publicações e toda divulgação referente aos eventos. Além disso, trabalha juntamente com a equipe de Eventos, a fim de apoiar no que for preciso.</blockquote>
                    <div class="row">
                        <figure class="col-md-4 col-md-offset-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/roni.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Ronie Santana</strong>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </section>
            </div>        
        </div>
        
        <!-- TI  -->
        <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Tecnologia da Informação</h3>
                    <blockquote>Colaboram no desenvolvimento e na manutenção do site e do aplicativo da FEMEC. Além disso, realizam o suporte aos projetos da FEMEC que envolvam tecnologia.</blockquote>
                    <div class="row">
                        
                        <figure class="col-md-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/anne.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Anne Kelly</strong>
                                </div>
                            </figcaption>
                        </figure>

                        <figure class="col-md-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/guilherme.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Guilherme Silva</strong>
                                </div>
                            </figcaption>
                        </figure>

                        <figure class="col-md-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/joyce.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Joyce Souza</strong>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </section>
            </div>
        </div>
    </div><br>    

    <div class="row">
        
        <!-- ESPORTES  -->
        <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Esportes</h3>
                    <blockquote>Atuam na organização das atividades esportivas dos eventos da FEMEC. </blockquote>
                    <div class="row">
                        
                        <figure class="col-md-4 col-md-offset-2 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/brenio.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Brenio Silva</strong>
                                    
                                </div>
                            </figcaption>
                        </figure>

                        <figure class="col-md-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/mateusrocha.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Mateus Rocha</strong>
                                </div>
                            </figcaption>
                        </figure>
                        
                    </div>
                </section>
            </div>
        </div>

        <!-- MIDIA  -->
        <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Mídia</h3>
                    <blockquote>Responsáveis por atividades ligadas a fotografia, projeção e sonoplastia. </blockquote>
                    <div class="row">
                        
                        <figure class="col-md-4 col-md-offset-2 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/andrematheus.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>André Matheus</strong>
                                    
                                </div>
                            </figcaption>
                        </figure>

                        <figure class="col-md-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/reinato.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Reinato Oliveira</strong>
                                    
                                </div>
                            </figcaption>
                        </figure>
                        
                    </div>
                </section>
            </div>
        </div>
    </div> <br>
    
        
    <div class="row">
        <!-- Diretoria Administrativa  -->
        <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Diretoria Administrativa</h3>
                    <blockquote>Supervisionar e colaborar com os departamentos e tratativas da matriz de responsabilidade e municiar o presidente para execuções dos projetos.</blockquote>
                    <div class="row">
                        <figure class="col-md-4 col-md-offset-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/caio.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Alyson Caio</strong>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </section>
            </div>        
        </div>
    </div>
</section>
</div>




