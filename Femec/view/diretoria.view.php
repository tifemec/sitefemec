<div id="wrapper" class="bg-white">
    <section class="container">
        <h1>Conheça a nossa <b>Diretoria</b></h1></br>

        <div class="row">

            <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Presidência</h3>
                    <blockquote>Tem como função acompanhar, dirigir e controlar o planejamento de todas as áreas. Seu foco principal é zelar pela área espiritual da Federação.</blockquote>
                    <div class="row">
                        <figure class="col-md-4 col-md-offset-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/tiago.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Tiago Bispo</strong><br>
                                    <strong>IEC Cavaleiro</strong>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </section>
            </div>        
            </div>

            <div class="col-md-6">
            <div class="department">
                <section>
                    <h3 class="text-center">Conselheiro</h3>
                    <blockquote>Sua função é acompanhar o trabalho das Federação nos aspectos de logística, suporte espiritual e viabilizar os recursos humanos e espirituais.</blockquote>
                    <div class="row">
                        <figure class="col-md-4 col-md-offset-4 text-center">
                            <figcaption>
                                <img src="assets/images/diretoria/pastor.jpg" class="img-rounded img-responsive">
                                <div class="">
                                    <strong>Pr Paulo Bispo</strong><br>
                                    <strong>IEC Cavaleiro</strong>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </section>
            </div>        
            </div>

        </div>

        <div class="divider"></div>
        <div class="row">

            <div class="col-md-6">
                <div class="department">
                    <section>
                        <h3 class="text-center">Tesouraria</h3>
                        <blockquote>Atuam administração das finanças da FEMEC (controle de entradas e saídas) e dos relatórios do balanço financeiro.</blockquote>
                        <div class="row">
                            <figure class="col-md-4 col-md-offset-2 text-center">
                                <figcaption>
                                    <img src="assets/images/diretoria/marcos.jpg" class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Marcos Paulo</strong><br>
                                        <strong>IEC Cavaleiro</strong><br>
                                        <strong>1º Tesoureiro</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/diretoria/danilo.jpg" class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Danilo Dutra</strong><br>
                                        <strong>IEC Cavaleiro</strong><br>
                                        <strong>2º Tesoureiro</strong>
                                    </div>
                                </figcaption>
                            </figure>
                            
                        </div>
                    </section>
                </div>
            </div>

            
            <div class="col-md-6">
                <div class="department">
                    <section>
                        <h3 class="text-center">Secretaria</h3>
                        <blockquote>Atuam na gestão da Agenda FEMEC junto às UMECs, além de confeccionar e organizar ofícios, cartas convites e documentos da federação.</blockquote>
                        <div class="row">
                            <figure class="col-md-4 col-md-offset-2 text-center">
                                <figcaption>
                                    <img src="assets/images/diretoria/juliana.jpg" class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Juliana Ferreira</strong><br>
                                        <strong>IEC Tamarineira</strong><br>
                                        <strong>1ª Secretária</strong>
                                    </div>
                                </figcaption>
                            </figure>

                            <figure class="col-md-4 text-center">
                                <figcaption>
                                    <img src="assets/images/diretoria/fernanda.jpg" class="img-rounded img-responsive">
                                    <div class="">
                                        <strong>Fernanda Teles</strong><br>
                                        <strong>IEC Afogados</strong><br>
                                        <strong>2ª Secretária</strong>
                                    </div>
                                </figcaption>
                            </figure>
                            
                        </div>
                    </section>
                </div>
            </div>

        </div>

         <div class="divider"></div>    

    </section>
</div>
