<div id="wrapper" class="bg-white">

  <section class="container">
    <h1>Conheça um pouco sobre o  projeto <b>Nilson Braga</b></h1>
    <div class="container">

      <section>
            <h3><b>Primeira reunião entre FEMEC e diretorias das UMECS de 2018</b>
              <small>03 de Março de 2018</small>
            </h3>
            <blockquote>Nossa reunião ocorreu na IEC Cavaleiro e teve início às 17h. Contamos com a participação de 14 igrejas da nossa denominação, representadas por suas lideranças dos jovens.
            </br><br> Nesta tarde, tratamos de alguns assuntos como: momento aberto para feedbacks sobre 2017 e suas dificuldades; percepção de que os jovens estão atendendo ao chamado (como esperado a partir do Congresso Chamada Perdida em 2017); apresentação do novo site, nova diretoria FEMEC, proposta de Aplicativo da Federação e novas camisas da FEMEC.
            </br><br> Ainda durante a reunião, construímos juntos o Calendário FEMEC 2018, contendo as programações da própria Federação e também incluindo as atividades abertas e fechadas das UMECS.
            </br><br> Além disso, foi apresentado o Congresso e Acampamento Mova-se, que ocorrerá nos dias 12, 13 e 14 de Outubro/2018 na Chácara Celeiro. Podemos louvar ao Senhor juntos pelos Seus grandes feitos em nosso meio e na organização desse acampamento, podendo perceber Sua provisão e bondade em todo tempo. Discutimos também detalhes como: algumas regras e esboço de programação dos dias reunidos na Chácara.
            <br><br> Por fim, foi reapresentado o Plano de Sócios Sou+Femec, que tem por objetivo ajudar os custos da Federação e em especial facilitar a ida dos jovens ao Acampamento Mova-se.
            </blockquote>
        </section>

      <section>


        <blockquote>O Projeto, que hoje tem o nome de Nilson Braga, nasceu no ano de 1999 e se chamava originalmente “Férias para Jesus”. Seu idealizador foi o Pr. Nilson Ferreira Braga, da IEC Cuiabá/MT, campo missionário do Departamento de Evangelização e Missões da UIECB. A proposta era de que as pessoas dedicassem parte de suas férias à obra missionária para instalação ou fortalecimento de um campo. Além da evangelização, o projeto deveria ter alcance social. A ideia foi apresentada pelo
        Pr.Nilson na 45ª Assembleia Geral da UIECB, em Guarapari/ES, no ano de 1999 e teve plena aceitação. <br> Mas aprouve ao Senhor chamar o Pr. Nilson a sua presença, quando viajava de carro de Guarapari para São Gonçalo/RJ, ao final da assembleia,  onde pretendia, junto com sua esposa, irmã Lília Braga, visitar familiares e sua antiga igreja (IEC Sete Pontes). Vítima de um acidente, ele faleceu na hora e sua esposa ficou bastante ferida, especialmente nos pés, mas sobreviveu. </br><br>Deus mudou
        a liderança do projeto, mas não permitiu que deixasse de ser realizado. Sob a direção da Missionária Sandra Roger, nomeada pelo então presidente da Junta Geral, Pr. Paulo Leite da Costa, o projeto aconteceu na cidade de Terra Nova do Norte, MT, no mês de julho daquele mesmo ano e dele participaram mais de 350 pessoas, a maior parte seguindo de ônibus (6) de cidades do Estado do Rio de Janeiro como Rio, Niterói, São Gonçalo, Itaboraí etc. Além de evangelismo e ação social, foi
        possível, também, construir um templo em terreno doado pela prefeitura da cidade. <br></br> Em homenagem ao seu idealizador, o projeto teve o nome trocado para PROJETO NILSON BRAGA a partir de então. Este ano estará sendo realizada a 19ª mobilização, tendo a cidade de Canavieiras/BA como destino e onde já existe um campo missionário em funcionamento. A ação missionária estará fortalecendo o campo e promovendo a construção do salão de cultos no terreno que foi adquirido e já totalmente
        quitado.</blockquote>

        <div class="divider"></div>
        <img src="assets/images/nilson/igreja.jpg"  class="img-responsive img-rounded col-md-3">
        <img src="assets/images/nilson/diretoria.jpg" class="img-responsive img-rounded col-md-3">
        <img src="assets/images/nilson/carro.jpg"  class="img-responsive img-rounded col-md-3">
        <img src="assets/images/nilson/criancas.jpg"  class="img-responsive img-rounded col-md-3">
      </section>

    </div>

    
    <div class="divider"></div>
    <blockquote><a target="_blank" style="color: #1696a5;" href="https://demcongregacional.com.br/projeto-nilson-braga-o-que-e/"> Conheça mais sobre o Projeto Nilson Braga no site oficial do Departamento de Evangelização e Missões, da UIECB!</a></blockquote>
  </section>
</div>
