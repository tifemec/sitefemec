<div id="wrapper" class="bg-white">

  <section class="container">
  <img src="assets/images/cultos-femec/background-lute.png"  class="img-responsive img-rounded center">
  <br>
      <h1>CULTO FEMEC: <b>LUTE</b></h1><br><br>
      <div class="container">
          <section>
            <blockquote>Em 2018, nossa federação estará promovendo uma série de cultos temáticos, mais conhecida como <b>Na Estrada Com a FEMEC</b>. O primeiro destes encontros terá por tema: <b>LUTE</b>! Sua ideia principal é estimular os jovens a <b>lutar contra o pecado e a buscar mais fome do Senhor Jesus.</b>
                </br><br> Nessa ocasião, teremos momentos maravilhosos na presença de Deus e dos nossos irmãos, podendo desfrutar de muito louvor, dança, teatro, Palavra e uma cantina espetacular!
                </br><br> <b>A Federação aguarda você!</b> 
                </blockquote>
          </section>
          
          <div class="divider"></div>
          
          <h1>Como, Quando e Onde? </h1>
          <section>
            <blockquote>O <b>Culto LUTE</b> ocorrerá no dia <b> 07 de Abril de 2018, às 19h, na 
              <a target="_blank" style="color: #1696a5;" href="https://goo.gl/maps/wnypQEpeQzD2">Igreja Evangélica Congregacional do Ibura</a>!</b>
              Estarão na Organização deste grande encontro as <b>UMECs Ibura e Jordão</b>, mas contamos com a <b>participação de todas as UMECs</b>, a fim de louvar ao nosso Deus juntos e crescer no conhecimento da Sua Palavra.
            </blockquote>
          </section>
      
        <div class="divider"></div>

        <blockquote>
          <b>Procure seu Líder da UMEC ou a Diretoria da FEMEC para maiores detalhes e participe! VAMOS À LUTA! </b>
        </blockquote>

  </section>
</div>
