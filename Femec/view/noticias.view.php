<div id="wrapper" class="bg-white">

  <section class="container">
    <br><h1>Veja aqui as notícias mais importantes da FEMEC!</h1><br><br>

    <div class="container">
      <section>
            <br><br><h2 style="color: #1696a5;"><i>Primeira reunião entre FEMEC e diretorias das UMECS de 2018</i>
            </br><b> <small>03 de Março de 2018</small></b></h2>
            
            <blockquote>Nossa reunião ocorreu na IEC Cavaleiro e teve início às 17h. Contamos com a participação de 14 igrejas da nossa denominação, representadas por suas lideranças dos jovens.
              </br><br> Nesta tarde, tratamos de alguns assuntos como: momento aberto para feedbacks sobre 2017 e suas dificuldades; percepção de que os jovens estão atendendo ao chamado (como esperado a partir do Congresso Chamada Perdida em 2017); apresentação do novo site, nova diretoria FEMEC, proposta de Aplicativo da Federação e novas camisas da FEMEC.
              </br><br> Ainda durante a reunião, construímos juntos o 
                <a target="_blank" style="color: #1696a5;" href="https://1drv.ms/b/s!Aqq01Yn2Zhx3i2yO33zOe9WrtJJP"> Calendário FEMEC 2018</a>,
                 contendo as programações da própria Federação e também incluindo as atividades abertas e fechadas das UMECS.
              </br><br> Além disso, foi apresentado o Congresso e Acampamento Mova-se, que ocorrerá nos dias 12, 13 e 14 de Outubro/2018 na Chácara Celeiro. Podemos louvar ao Senhor juntos pelos Seus grandes feitos em nosso meio e na organização desse acampamento, podendo perceber Sua provisão e bondade em todo tempo. Discutimos também detalhes como: algumas regras e esboço de programação dos dias reunidos na Chácara.
              <br><br> Por fim, foi reapresentado o Plano de Sócios Sou+Femec, que tem por objetivo ajudar os custos da Federação e em especial facilitar a ida dos jovens ao Acampamento Mova-se.
            </blockquote>
      </section>

      <section>
        <img src="assets/images/noticias/reuniao0303-1.jpeg"  class="img-responsive img-rounded col-md-3">
        <img src="assets/images/noticias/reuniao0303-2.jpeg" class="img-responsive img-rounded col-md-3">
        <img src="assets/images/noticias/reuniao0303-3.jpeg"  class="img-responsive img-rounded col-md-3">
        <img src="assets/images/noticias/reuniao0303-4.jpeg"  class="img-responsive img-rounded col-md-3">
      </section>

      <div class="divider"></div> 

    </div>
  </section>

</div>

<!--https://1drv.ms/b/s!Aqq01Yn2Zhx3i161NeiB6NuIQjQL-->