<div id="wrapper" class="bg-white">
    <h1>Quem somos</h1>
    <h2 class="sub-title"></h2>

    <div class="container">
        <section>
            <h2><span class= "glyphicon glyphicon-globe text-success"aria-hidden="true"></span>Nossa Missão</h2>
            <blockquote>Contribuir para o trabalho do reino de Deus, dando subsídios e orientações para o trabalho das UMECs de acordo com as peculiaridades de cada uma, incentivando a harmonia e a união entre as UMECs, estimular e desenvolver os líderes e as uniões de jovens a perseverarem no amor pela causa de Deus; de modo a cumprir o legado deixado por Cristo a todos seus discípulos: ser Testemunha e Sal da Terra.</blockquote>
        </section>

        <section>
            <h2><span class= "glyphicon glyphicon-search text-danger"aria-hidden="true"></span>Nossa Visão</h2>
            <blockquote>Ser uma federação de forte atuação entre as igrejas desta Associação Koinonia  da União das Igrejas Evangélicas Congregacionais do Brasil.</blockquote>
        </section>

        <section>
            <h2><span class= "glyphicon glyphicon-hand-right"aria-hidden="true"></span>Versículo chave</h2>
            <blockquote>O Senhor é a minha força, o meu cântico e a minha salvação.<small>Êxodo 15:02</small></blockquote>
        </section>
    </div>
</div>
