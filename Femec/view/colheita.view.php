<div id="wrapper" class="bg-white">

  <section class="container">
      <h1>Conheça um pouco sobre o <b>PROJETO COLHEITA</b></h1><br></BR>

      <div class="container">
          <section>
            <blockquote><b>Projeto Colheita</b> é o nome adotado para divulgação e marketing do Plano Decenal de Crescimento (2008-2018) da União das Igrejas Evangélicas Congregacionais do Brasil – UIECB. É um projeto que propõe fortalecer a unidade para gerar crescimento, entendendo que cada um tem participação ativa neste processo de multiplicação de igrejas.
            <br><br>O acróstico que segue ajuda a entender  a visão, as estratégias e a meta para a realização do Projeto Colheita.<br>
              <blockquote><b>C</b> omprometer-se com a visão denominacional de crescimento </blockquote>
              <blockquote><b>O</b> rar pelo projeto e pela salvação das almas </blockquote>
              <blockquote><b>L</b> evantar os recursos necessários à execução </blockquote>
              <blockquote><b>H</b> abilitar os crentes para evangelização e discipulado </blockquote>
              <blockquote><b>E</b> xecutar as metas propostas nos fóruns </blockquote>
              <blockquote><b>I</b> nvestir no discipulado dos novos convertidos </blockquote>
              <blockquote><b>T</b> rabalhar unidos por um fim comum </blockquote>
              <blockquote><b>A</b> largar as tendas, triplicar até 2018</blockquote>
            </blockquote>
          </section>

          <div class="divider"></div>
          <h1>OBJETIVOS</h1>
          <blockquote>Pregar o evangelho de nosso Senhor Jesus Cristo (Mc 16.15) mostrando ao mundo que a salvação está somente nEle (At 4.12).</blockquote>
          <blockquote>Promover uma semeadura eficaz da Palavra de Deus para colhermos com abundância.</blockquote>
          <blockquote>Mobilizar cada crente, igreja, associação, confederação e todos os departamentos da UIECB para a implantação de mais de 700 novas igrejas congregacionais no período de 10 anos, 2008 a 2018.</blockquote>
<!--
              <div class="col-md-4 ">
                  <figure class="quote quote-md">
                      <blockquote></blockquote>
                      <figcaption class="row">
                          <img src="assets/images/dem/jay.jpg" class="img-responsive img-rounded col-md-4">
                          <div class="col-md-7">
                              <div>Pr. Ialan Jay de Sá Cavalcante </div>
                              <strong>Presidente</strong>
                          </div>
                      </figcaption>
                  </figure
              </div>

              <div class="col-md-4">
                  <figure class="quote quote-md">
                      <blockquote></blockquote>
                      <figcaption class="row">
                          <img src="assets/images/dem/antonio.jpg" class="img-responsive img-rounded col-md-4">
                          <div class="col-md-7">
                              <div>Pr. Antônio Carlos Ramos </div>
                                  <strong>Secretário</strong>
                          </div>
                      </figcaption>
                  </figure>
              </div>

              <div class="col-md-4">
                  <figure class="quote">
                      <blockquote class="quote-sm"></blockquote>
                      <figcaption class="row">
                          <img src="assets/images/dem/francisco.jpg" class="img-responsive img-rounded col-md-4">
                          <div class="col-md-7">
                              <div>Pr. Francisco Assis Morais Leite Filho </div>
                              <strong>Tesoureiro</strong>
                          </div>
                      </figcaption>
                  </figure>
              </div>

              <div class="divider"></div>
              <blockquote>Nosso Lema: “Para que todos os povos da terra saibam que o Senhor é Deus e que não há outro” (1º RS. 8.60).</blockquote>
              <blockquote>Nossa Visão: Alcançar os povos.</blockquote>
              <blockquote>Nossa Missão: Servir às Igrejas em seu agir missionário.</blockquote>
-->
        </div>
    </section>
</div>
