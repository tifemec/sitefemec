<div id="wrapper" class="bg-white">

  <section class="container">
  <img src="assets/images/demo/revolution_slider/slide-home/celeiros.jpg"  class="img-responsive img-rounded center">
  <br>
      <h1>Congresso <b>MOVA-SE</b></h1><br>
      <div class="container">
          <section>
            <blockquote>Esse Congresso de Jovens acontecerá nos dias <b>12, 13 e 14 de Outubro de 2018</b>, no espaço <b>Celeiros Eventos</b>, localizado na BR-101 Km57 Norte, Bairro Guabiraba, Recife - PE.
                </br><br>O objetivo deste Congresso é que nossos jovens aprendam <b>o que lhes impede de se mover, de levar o evangelho e de compartilhar sua fé</b>.
                Assim, eles verão o poder que cada um deles tem para <b>mudar o mundo através de Jesus</b> e do <b>compartilhar do amor de Deus</b>.
                </br><br> Vamos juntos desfrutar do seu e do nosso Acampamento! Invista no seu ministério para que possamos descobrir, em conjunto, a vontade de Deus em nossas vidas e refletir diariamente o que estamos deixando de fazer! 
                          Com certeza, Deus tem preparado momentos inexplicáveis para os nós durante aqueles dias!
                </br><br> <b>A Federação aguarda você!</b> 
                </blockquote>

                <iframe width="560" height="315" src="https://www.youtube.com/embed/eysZappTrmE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          </section>
        
          <div class="divider"></div>

          <h1>Como participar? </h1>
          <section>
            <blockquote>Para facilitar a ida do maior número de jovens, nossa equipe preparou o maravilhoso 
              <a target="_blank" style="color: #1696a5;" href="https://www.youtube.com/watch?v=CEnePHNdBSM">Plano de Sócios SOU+Femec</a>! Você poderá escolher um dos três planos que combine melhor com você e aproveitar de diversos benefícios!
              </br><br> Confira abaixo os valores do nosso Plano de Sócios e também do Pacote Completo:

            </blockquote>

            <img src="assets/images/congresso-movase/valores-planos-de-socios.png"  class="img-responsive img-rounded col-md-offset-3">
            <br><br>
            <img src="assets/images/congresso-movase/pacote-completo.png"  class="img-responsive img-rounded col-md-offset-3">

            <blockquote>O investimento para o acampamento no Pacote Simples (sem Plano de Sócios) custa <b> R$ 180,00 à vista</b> e <b>R$ 210,00 parcelado</b>. 
              </br><br>Além das opções de pagamento usando <b>cartões de crédito e depósitos bancários</b>, também é possível escolher a opção do 
              <a target="_blank" style="color: #1696a5;" href="https://pag.ae/bbwqvPb">PagSeguro para Pacote Simples</a> ou 
              <a target="_blank" style="color: #1696a5;" href="https://pag.ae/bgwqv4z">PagSeguro para Plano Platinum</a>!
            </blockquote>
            <!--<b><a target="_blank" style="color: #2E363F; float:right;" href="http://uiecb.com.br/a-historia/"> Saiba mais sobre a história completa da UIECB no site oficial da União das Igrejas Evangélicas Congregacionais do Brasil!</a></b>-->
          </section>
      
        <!--<div class="divider"></div>

        <h1>Onde será? </h1>
          <section>
            <blockquote>O Congresso MOVA-SE será realizado no espaço <b>Celeiros Eventos</b>, localizado em: Tv. Padre Mosca de Carvalho - Guabiraba, Recife - PE, 52490-010.
              </br><br>Em breve fotos oficiais do local!
            </blockquote>

          </section>-->
      
        <div class="divider"></div>

        <blockquote>
          <b>Procure seu Líder da UMEC ou a Diretoria da FEMEC para esclarecimentos das vagas (que estão acabando) e faça já sua reserva! 
          </br>MOVA-SE AGORA! </b>
        
        <!--</br><br><a  style="color: #1696a5;" href="#"> Em breve, conheça mais sobre o Congresso MOVA-SE através do vídeo oficial do evento!</a></blockquote>-->

  </section>
</div>
