<div id="wrapper" class="bg-white">

    <section class="container">
        <h1>Conheça um pouco sobre o <b>DEM</b></h1><br></BR>

        <div class="container">
            <section>
                <blockquote>DEM é o <b>Departamento de Evangelização e Missões</b>, da UIECB (União das Igrejas Evangélicas Congregacionais do Brasil) que tem os seguintes objetivos:</br>
                <blockquote><i>Coordenar e agenciar o trabalho missionário da União no âmbito Nacional e mundial;</i></blockquote>
                <blockquote><i>Motivar as Igrejas no desempenho da tarefa missionária;</i></blockquote>
                <blockquote><i>Preparar e distribuir material próprio para o trabalho de evangelização</i></blockquote>
                </blockquote>
            </section>

            <div class="divider"></div>

            <h1>Sobre o DEM</h1>
            <blockquote>O DEM tem um Conselho composto de nove conselheiros, com renovação de um terço a cada Assembleia Geral da União. Possui uma diretoria composta de no mínimo, um diretor, um secretário e um tesoureiro, sendo estes membros do Conselho. A fim de operacionalizar melhor sua ação denominacional, o DEM conta com um secretário executivo contratado pelo departamento e com seu nome homologado em reunião da Junta Geral.<br></br>Hoje somos cerca de 52 Campos Missionários no Brasil e na América Latina e temos missionários associados na África, Europa e Ásia.
            <br>Está sob a coordenação do DEM o Projeto Colheita, um plano decenal de crescimento denominacional que foi lançado em 2008 e se estende até 2018.</blockquote>

            <div class="col-md-4 ">
                <figure class="quote quote-md">
                    <blockquote></blockquote>
                    <figcaption class="row">
                        <img src="assets/images/dem/jay.jpg" class="img-responsive img-rounded col-md-4">
                        <div class="col-md-7">
                            <div>Pr. Ialan Jay de Sá Cavalcante </div>
                            <strong>Presidente</strong>
                        </div>
                    </figcaption>
                </figure>
            </div>

            <div class="col-md-4">
                <figure class="quote quote-md">
                    <blockquote></blockquote>
                    <figcaption class="row">
                        <img src="assets/images/dem/antonio.jpg" class="img-responsive img-rounded col-md-4">
                        <div class="col-md-7">
                        <div>Pr. Antônio Carlos Ramos </div>
                        <strong>Secretário</strong>
                        </div>
                    </figcaption>
                </figure>
            </div>

            <div class="col-md-4">
                <figure class="quote">
                    <blockquote class="quote-sm"></blockquote>
                    <figcaption class="row">
                        <img src="assets/images/dem/francisco.jpg" class="img-responsive img-rounded col-md-4">
                        <div class="col-md-7">
                            <div>Pr. Francisco Assis Morais Leite Filho </div>
                            <strong>Tesoureiro</strong>
                        </div>
                    </figcaption>
                </figure>
            </div>

            <div class="divider"></div>

            <h1>Princípios</h1>
            <blockquote><b>Nosso Lema:</b> “Para que todos os povos da terra saibam que o Senhor é Deus e que não há outro” (1º RS. 8.60).</blockquote>
            <blockquote><b>Nossa Visão:</b> Alcançar os povos.</blockquote>
            <blockquote><b>Nossa Missão:</b> Servir às Igrejas em seu agir missionário.</blockquote>

        </div>

        <div class="divider"></div>
        <blockquote><a target="_blank" style="color: #1696a5;" href="https://demcongregacional.com.br/"> Conheça mais sobre o DEM no site oficial do Departamento de Evangelização e Missões, da UIECB!</a></blockquote>
        
  </section>
</div>
