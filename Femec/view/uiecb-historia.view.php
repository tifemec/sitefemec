<div id="wrapper" class="bg-white">

  <section class="container">
      <h1>Conheça um pouco sobre a <b>UIECB</b></h1><br><br>
      <img src="assets/images/uiecb-oficial.png"  class="img-responsive img-rounded col-md-2">
      <div class="container">
          <section>
            <blockquote>A <b>UIECB</b> representa a União das Igrejas Evangélicas Congregacionais do Brasil. Hoje com 400 igrejas associadas e 637 ministros ordenados e duas instituições teológicas: Seminário Teológio Congregacional do Rio de Janeiro (STCRJ) e Seminário Teológio Congregacional do Nordeste (STCN).
            </br><br>Este órgão tem como missão: promover cooperação mútua em diversas áreas como o evangelismo, missões nacionais e estrangeiras, educação religiosa e teológica, atividades ministeriais etc.</blockquote>
          </section>
          
          <div class="divider"></div>
          
          <h1>Nossa História </h1>
          <section>
            <blockquote>A origem da UIECB está no trabalho missionário realizado pelo casal Robert Reid Kalley e Sarah Poulton Kalley, que chegaram à cidade do Rio de Janeiro, capital do Império do Brasil, em 10 de maio de 1855 para iniciarem nessa no Brasil um trabalho que duraria 21 anos e 57 dias.</h3>
              </br><br>O casal Kalley, chegado ao Rio, foi instalar-se em Petrópolis, numa casa conhecida como GERNHEIM, que significa, “Lar muito amado”. Nessa mesma casa, em 19 de agosto de 1855, um domingo à tarde, Kalley e sua esposa instalaram a primeira classe de Escola Dominical, contando com cinco crianças, filhos de cidadãos americanos. Foi contada a história do profeta Jonas.
              </br><br>Com o desenvolvimento do trabalho, Kalley escreveu a amigos e antigos companheiros que estavam em Ilinnois, convidando-os a virem auxiliá-lo no Brasil. O primeiro a chegar foi Wiliam Deatron Pitt, jovem inglês que havia sido.
              </br><br>O primeiro crente batizado pelo Dr. Kalley foi o português José Pereira de Souza Louro, em 8 de novembro de 1857. Mas foi em 11 de julho de 1858 que ele organizou a primeira igreja evangélica de regime congregacionalista no Brasil: A Igreja Evangélica Fluminense. Foi organizada com 14 membros tendo sido batizado naquele dia o sr. Pedro Nolasco de Andrade, primeiro brasileiro batizado por Kalley. 
              </br><br>Evangelistas e colportores preparados por Kalley espalharam a mensagem do Evangelho por diferentes áreas do país, chegando à região do Nordeste, propriamente na cidade do Recife em Pernambuco, onde o próprio Kalley organizou a Igreja Evangélica Pernambucana no ano de 1873 e, à partir destas duas igrejas, outras foram implantadas em outras cidades.
            </blockquote>
            <b><a target="_blank" style="color: #2E363F; float:right;" href="http://uiecb.com.br/a-historia/"> Saiba mais sobre a história completa da UIECB no site oficial da União das Igrejas Evangélicas Congregacionais do Brasil!</a></b>
          </section>
      
        <div class="divider"></div>
        <blockquote><a target="_blank" style="color: #1696a5;" href="http://uiecb.com.br/"> Conheça mais sobre a UIECB no site oficial da União das Igrejas Evangélicas Congregacionais do Brasil!</a></blockquote>

  </section>
</div>
