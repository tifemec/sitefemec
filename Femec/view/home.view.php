<div id="wrapper" >

    <div class="fullwidthbanner-container roundedcorners">
        <div class="fullwidthbanner">
          <ul>
              <!-- SLIDE  -->
              <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                  <img src="assets/images/demo/revolution_slider/slide-home/sliderbg.jpg" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                  <div class="tp-caption lightgrey_divider skewfromrightshort customout" data-x="85" data-y="224" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="1200" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off" style="z-index: 2">

                  </div>

                  <div class="tp-caption customin customout" data-x="right" data-hoffset="-220" data-y="bottom" data-voffset="0"
                      data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="800" data-start="700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn"
                      style="z-index: 3">
                          <img src="assets/images/demo/revolution_slider/slide-home/desktop_slider_2.png">
                  </div>

                  <div class="tp-caption customin customout" data-x="right" data-hoffset="-100" data-y="bottom" data-voffset="0"
                      data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1500" data-start="700" data-easing="Elastic.easeInOut" data-endspeed="500" data-endeasing="Power1.easeIn"
                      style="z-index: 3">
                          <img src="assets/images/demo/revolution_slider/slide-home/slider_run.png">
                  </div>

                  
                  <div class="tp-caption large_bold_grey skewfromrightshort customout" data-x="80" data-y="66"
                      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off"
                      style="z-index: 4">FEMEC
                  </div>

                  <div class="tp-caption large_bold_grey skewfromleftshort customout" data-x="80" data-y="122"
                      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="300" data-start="1100" data-easing="Back.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off"
                      style="z-index: 7">Koinonia
                  </div>

                  <div class="tp-caption small_thin_grey customin customout" data-x="80" data-y="230"
                      data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off"
                      style="z-index: 8">Estamos preparando um site novinho<br>com a cara dos nossos jovens e muito mais informações!<br><strong>Em breve maiores novidades! Acesse e divulgue!</strong>
                  </div>
                  
                  <a target="_blank" href="/noticias">
                    <div class="tp-caption large_bold_darkblue skewfromleftshort customout" data-x="80" data-hoffset="-90" data-y="350"
                      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="1600" data-easing="Back.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off"
                      style="z-index: 10">Notícias
                    </div>
                  </a>
                
                  <div class="tp-caption medium_bg_orange skewfromleftshort customout" data-x="230" data-hoffset="-90" data-y="410"
                      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2300" data-easing="Back.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="off"
                      style="z-index: 21" >Com atualizações mensais
                </div>
              </li>

              <!-- SLIDE -->
              <li data-transition="incube-horizontal" data-slotamount="5" data-masterspeed="500" >
                
                  <img src="assets/images/demo/revolution_slider/slide-home/bannerplanosocios.png" alt="" data-bgfit="cover" data-bgposition="top" data-bgrepeat="no-repeat">
                    
                  <div class="tp-caption large_text sft" data-x="center" data-y="360"
                      data-customin="x:-150;y:-280;z:0;rotationX:-140;rotationY:-280;rotationZ:-220;scaleX:1.5;scaleY:0;skewX:57;skewY:60;opacity:0;transformPerspective:600;transformOrigin:120% 190%;" data-customout="x:-450;y:-430;z:-30;rotationX:90;rotationY:-290;rotationZ:-20;scaleX:1;scaleY:2.8;skewX:59;skewY:4;opacity:0;transformPerspective:600;transformOrigin:70% 0%;" data-speed="1500" data-endspeed="500" data-start="2700" data-endeasing="Bounce.easeOut"
                      data-easing="Back.easeInOut">
                      <!-- Botao Saiba Mais-->
                      <a class="btn btn-danger btn-lg" target="_blank" href="https://www.youtube.com/watch?v=CEnePHNdBSM">SAIBA MAIS!</a>
                      
                  </div>
                
              </li>

              <li data-transition="incube-horizontal" data-slotamount="5" data-masterspeed="500"  >
                  
                    <img src="assets/images/demo/revolution_slider/movase.jpg" alt="" data-bgfit="cover" data-bgposition="top" data-bgrepeat="no-repeat" margin-left = "50%" margin-right= "-50%" display = "block">    

                  <div class="tp-caption large_text sft" data-x="center" data-y="360"
                      data-customin="x:-150;y:-280;z:0;rotationX:-140;rotationY:-280;rotationZ:-220;scaleX:1.5;scaleY:0;skewX:57;skewY:60;opacity:0;transformPerspective:600;transformOrigin:120% 190%;" data-customout="x:-450;y:-430;z:-30;rotationX:90;rotationY:-290;rotationZ:-20;scaleX:1;scaleY:2.8;skewX:59;skewY:4;opacity:0;transformPerspective:600;transformOrigin:70% 0%;" data-speed="1500" data-endspeed="500" data-start="2700" data-endeasing="Bounce.easeOut"
                      data-easing="Back.easeInOut">

                      <!-- Botao Saiba Mais-->
                     <a class="btn btn-danger btn-lg" target="_blank" href="/congresso-movase">SAIBA MAIS!</a>
                  </div>
              </li>

             
          </ul>
          <br> <br>
        </div>
    </div>

    <div class="container">
        
    <!--Agenda Femec-->
        <div class="col-md-4 col-md-offset-4 text-center">
            <h3 class="font100" style="font-size: 1.5em;margin-bottom: 10px"> 
            <span class="fontbold"><b>Agenda FEMEC</b></span></h3>

            <div class="service-box eventos-emanuel img-rounded eventos-emanuel">
                <div class="widget widget-popular-posts">

                    <table class="table table-striped embed-responsive embed-responsive-16by9">
                        <tbody>
                            <tr>
                                <td style="font-size: 15px;font-weight: 600">03/03</td>
                                <td style="font-size: 13px">Reunião Geral- Diretoria de Mocidades</td>
                            </tr>
                            <!--<tr>
                            <td style="font-size: 15px;font-weight: 600">24/03</td>
                            <td style="font-size: 13px">Culto FEMEC - Tema: DIVERGENTES</td>
                            </tr>-->

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">07/04</td>
                            <td style="font-size: 13px">Culto FEMEC <br> Tema: LUTE <br>Local: IEC Ibura</td>
                            </tr>

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">19/05</td>
                            <td style="font-size: 13px">Culto FEMEC <br>Tema: DESINTOXICAÇÃO <br>Local: IEC Nazaré da Mata</td>
                            </tr>

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">16/06</td>
                            <td style="font-size: 13px">Culto FEMEC <br>Tema: ALCANÇANDO O IMPOSSÍVEL <br>Local: IEC Dois Carneiros</td>
                            </tr>

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">21/07</td>
                            <td style="font-size: 13px">Culto FEMEC <br>Tema: DIVERGENTES <br>Local: IEC Tamarineira</td>
                            </tr>

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">04/08</td>
                            <td style="font-size: 13px">Culto FEMEC <br>Tema: COMO NÃO MUDAR O MUNDO <br>Local: IEC Pernambucana</td>
                            </tr>

                            <tr>
                            <td style="font-size: 15px;font-weight: 600">29/09</td>
                            <td style="font-size: 13px">Culto FEMEC <br>Tema: INABALÁVEL <br>Local: IEC Afogados</td>
                            </tr>

                            <tr>
                                <td style="font-size: 15px;font-weight: 600">12,13,14/10</td>
                                <td style="font-size: 13px">Acampamento FEMEC <br>TEMA: MOVA-SE! IDE E FAZEI DISCÍPULOS!<br>Local: Chácara Celeiros</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <!--videos-->
        <!--<div class="row">

            <div class="col-md-2 col-md-offset-4 text-center">
                <h2></h2>
                <!-- YOUTUBE VIDEO 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube.com/watch?v=CEnePHNdBSM" frameborder="0" allowfullscreen="">Plano de Sócios</iframe>
                </div>

                <!-- YOUTUBE VIDEO 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube.com/watch?v=CEnePHNdBSM"></iframe>
                </div>
            </div>
        </div>-->
    </div>
        
</div>