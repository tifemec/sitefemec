<div id="wrapper">


  <form class="form-horizontal">
  <fieldset>

  <!-- Form Name -->
<h1>DEIXE-NOS UMA MENSAGEM</h1><br></br>
  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-4 control-label" for="textinput_nome">Nome</label>
    <div class="col-md-4">
    <input id="textinput_nome" name="textinput_nome" placeholder="digitar nome completo" class="form-control input-md" required="" type="text">
    </div>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-4 control-label" for="textinput_email">Email</label>
    <div class="col-md-4">
    <input id="textinput_email" name="textinput_email" placeholder="digite um email válido" class="form-control input-md" required="" type="text">
    </div>
  </div>

  <!-- Textarea -->
  <div class="form-group">
    <label class="col-md-4 control-label" for="textarea_mensagem">Mensagem</label>
    <div class="col-md-4">
      <textarea class="form-control" id="textarea_mensagem" name="textarea_mensagem"placeholder="digite aqui sua mensagem"></textarea>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
      <a class="btn btn-primary" href="home" role="button">Enviar</a>
  <div class="col-md-4">

    </div>
  </div>

  </fieldset>
  </form>

</div>
