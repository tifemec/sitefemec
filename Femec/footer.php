﻿<!-- FOOTER -->
<footer id="footer-page" style="margin-top:-20px">

    


    <!-- footer content -->
    <div class="footer-content">
        <div class="container">

            <div class="row">


                <!-- FOOTER CONTACT INFO -->
                <div class="column col-md-4">
                    <h3>CONTATOS</h3>

                    <p class="contact-desc">
                        Federação da Mocidade Evangélica Congregacional Koinonia
                    </p>
                    <address class="font-opensans">
                        <ul>
                            <li class="footer-sprite address">
                                Pernambuco<br />
                                Alagoas
                            </li>
                            <li class="footer-sprite phone">
                                Whatsapp: (81) 98184-7424
                            </li>
                            <li class="footer-sprite email">
                                <a href="mailto:femeckoinonia@hotmail.com"> femeckoinonia@hotmail.com</a>
                            </li>
                        </ul>
                    </address>

                </div>
                <!-- /FOOTER CONTACT INFO -->


                <!-- FOOTER LOGO -->
                <div class="column logo col-md-4 text-center">
                    <div class="logo-content">
                        <img class="animate_fade_in" src="assets/images/femec_logo.png" width="200" alt="" />
                    </div>
                </div>
                <!-- /FOOTER LOGO -->


                <!-- FOOTER LATEST POSTS -->
                <div class="column col-md-4 text-right">
                    <h3>Acesse também</h3>

                    <div class="post-item">
                        <small>COMEC</small>
                        <h3><a href="http://comecbrasil.com.br/">comecbrasil.com.br</a></h3>
                    </div>
                    <div class="post-item">
                        <small>EMCONE</small>
                        <h3><a href="http://emcone.com.br/">emcone.com.br</a></h3>
                    </div>
                    <div class="post-item">
                        <small>UIECB</small>
                        <h3><a href="http://uiecb.com.br/">uiecb.com.br/</a></h3>
                    </div>

                </div>
                <!-- /FOOTER LATEST POSTS -->

            </div>

        </div>
    </div>
    <!-- footer content -->

    <!-- copyright , scrollTo Top -->
    <div class="footer-bar">
        <div class="container">
            <span class="copyright">&copy; FEMEC Koinonia - Todos os direitos reservados</span>
            <!--<a class="toTop" href="#topNav">IR PARA O TOPO <i class="fa fa-arrow-circle-up"></i></a>-->
        </div>
    </div>
    <!-- copyright , scrollTo Top -->

</footer>
<!-- /FOOTER -->
