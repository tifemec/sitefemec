<?php
	require_once('functions.php');
	require_once('config.php');
?>

<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html lang="pt-br"> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title><?php echo TITLE; ?></title>

		<meta name="keywords" content="FEMEC,JOVENS,KOINONIA" />
		<meta name="description" content="" />
		<meta name="Author" content="FEMEC KOINONIA - Federação da Mocidade Evangélica Congregacional Koinonia" />

		<!-- mobile settings -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="view/public/img/favicon.ico">

		<!-- FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css" />
		<link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- PLUGINS -->
		<link href="assets/plugins/revolution-slider/css/settings.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/animate.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-responsive.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color_scheme/orange.css" rel="stylesheet" type="text/css" />

		<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />

		<!-- Morenizr -->
		<script type="text/javascript" src="assets/plugins/modernizr.min.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="assets/images/icon/realestate/femec_icone.png" >
</head>

<body class="bg-white">
	<?php
		include_once('header.php');

		//Definindo Controler
		$url = $_SERVER['REQUEST_URI'];

		$prepare = explode('/', $url);

		// valide
		if(count($prepare) > CODE_VIEW)
		{
			if($prepare[CODE_VIEW] != NULL)
			{
				$view = $prepare[CODE_VIEW];
			} else {
				$view = 'home';
			}
		} else {
			$view = 'home';

		}

		// load > View
		LoadView($view);

		include_once('footer.php');
	?>

	<!-- JAVASCRIPT FILES -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.appear.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.isotope.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.knob.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.stellar.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="assets/plugins/masonry.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="assets/plugins/mediaelement/build/mediaelement-and-player.min.js"></script>

	<!-- REVOLUTION SLIDER -->
	<script type="text/javascript" src="assets/plugins/revolution-slider/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="assets/plugins/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="assets/js/slider_revolution.js"></script>
	<script type="text/javascript" src="assets/js/scripts.js"></script>


	<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
	<!--<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-XXXXX-X', 'domainname.com');
		ga('send', 'pageview');
	</script>
	-->
</body>
</html>
